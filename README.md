# 手写spring-ioc框架

## 功能

	* 只简单的模仿ioc的控制反转功能，写了三个注解
	* @Component: 添加在类上,标明这个类交由容器创建
	* @Autowired: 添加在成员变量上,这个变量必须是已交给容器的bean
	* @BeanScan: 你希望扫描哪些包下的bean
	
## 使用说明

	* 框架核心代码只是annotation包和support包下的代码,上层目录包下的代码只是我写的测试类
	* 你必须创建一个 StartApplication 类,并为它添上注解@BeanScan
	* @Component和@Autowired就是如spring一样的普通功能,更加功能添加就好
	* StartContainer类是初始化框架,调用 new StartContainer()即表明启动框架