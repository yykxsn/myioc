package com.tczs.ioc;

import com.tczs.ioc.annotation.Component;

@Component
public class Cat {

    private String name;

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void say(){
        System.out.println(name+": my ioc success !");
    }

}
