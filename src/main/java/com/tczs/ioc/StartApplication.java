package com.tczs.ioc;

import com.tczs.ioc.annotation.BeanScan;
import com.tczs.ioc.support.LoadBean;
import com.tczs.ioc.support.StartContainer;


@BeanScan(scan = "com.tczs.ioc")
public class StartApplication {

    public static void main(String[] args) {

        StartContainer sc = new StartContainer();
        Cat cat = LoadBean.getBean(Cat.class);

        cat.setName("tom");

        People people = LoadBean.getBean(People.class);

        people.setName("litao");
        people.setAge(18);

        people.listenCatSay();

    }

}
