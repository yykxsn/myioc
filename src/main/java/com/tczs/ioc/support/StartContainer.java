package com.tczs.ioc.support;

/**
 * 启动spring-ioc容器
 * @author TaoerLi
 *
 */
public class StartContainer {

    public StartContainer() {
        init();
    }

    void init(){
        try {
            BeanScanPackage beanScanPackage = BeanScanPackage.getBeanScanPackage();

            LoadBean loadBean = new LoadBean();
            loadBean.loadBean(beanScanPackage.classList);

            InitBean initBean = new InitBean();
            initBean.initAll();
            System.out.println("spring-ioc容器启动成功");
        }catch (Exception e){
            e.printStackTrace();
            return;
        }
    }

}
