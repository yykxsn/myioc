package com.tczs.ioc.support;


import com.tczs.ioc.annotation.BeanScan;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 获取扫描路径并且加载路径下所有类
 * @author TaoerLi
 *
 */
public class BeanScanPackage {

    List<String> classPaths = new ArrayList<>();

    List<Class> classList = new ArrayList<>();

    public String allClassPath; //  /D:/WorkSpace/idea/myioc/target/classes/

    public String startApplicationClassPath; //StartApplication类的绝对路径

    private static BeanScanPackage beanScanPackage;

    private BeanScanPackage() throws ClassNotFoundException {
        init();
    }

    public static BeanScanPackage getBeanScanPackage() throws ClassNotFoundException {
        if(beanScanPackage == null){
            beanScanPackage = new BeanScanPackage();
        }
            return beanScanPackage;
    }

    private void init() throws ClassNotFoundException {
        findStartApplicationClass();
        String scanPath = getScanPath();
        searchClass(scanPath);
    }

    /**
     * 获取StartApplication类的绝对路径
     */
    private void findStartApplicationClass(){
        allClassPath = this.getClass().getClassLoader().getResource("").getPath();
        File file = new File(allClassPath);
        try {
            getStartApplicationClass(file);
        }catch (RuntimeException e){
            System.out.println(e);
        }
        if(startApplicationClassPath == null || startApplicationClassPath.length()==0){
            throw new RuntimeException("You must create StartApplication Class and add BeanScan annotation!");
        }
    }

    /**
     * 从StartApplication类的注解BeanScan上得到扫描路径
     * @return
     */
    public String getScanPath() throws ClassNotFoundException {
        String realClassName = convertClassPath(startApplicationClassPath,allClassPath);
        Class clazz = Class.forName(realClassName);

        if(clazz.isAnnotationPresent(BeanScan.class)){
            BeanScan anno = (BeanScan) clazz.getAnnotation(BeanScan.class);
            Method[] methods = anno.annotationType().getDeclaredMethods();
            for(Method method : methods ){
                if(!method.isAccessible()){
                    method.setAccessible(true);
                }
                try {
                    String scanPath = method.invoke(anno, null).toString();
                    if(scanPath == null || scanPath.length() == 0){
                        throw new RuntimeException("Scan of BeanScan can not is null or \"\"");
                    }
                    return scanPath;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        throw new RuntimeException("StartApplication class must have BeanScan annotation!");
    }

    /**
     * 查找扫描路径包下的所有Class,放入classList
     * @param
     * @throws ClassNotFoundException
     */
    public void searchClass(String scanPath) throws ClassNotFoundException {
        allClassPath = this.getClass().getClassLoader().getResource("").getPath();
       // String classpath = ScanPackage.class.getResource("/").getPath();
        scanPath = scanPath.replace(".", File.separator);
        String searchPath = allClassPath + scanPath;

        doPath(new File(searchPath));

        //这个时候我们已经得到了指定包下所有的类的绝对路径了
        for (String classPath : classPaths) {
            String className = convertClassPath(classPath,allClassPath);
            Class cls = Class.forName(className);
            classList.add(cls);
        }
    }


    private void doPath(File file) {

        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f1 : files) {
                doPath(f1);
            }
        } else {
            if (file.getName().endsWith(".class")) {
                classPaths.add(file.getPath());
            }
        }
    }

    private void getStartApplicationClass(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f1 : files) {
                getStartApplicationClass(f1);
            }
        } else {
            if (file.getName().indexOf("StartApplication.class") >= 0) {
                startApplicationClassPath = file.getPath();
                throw new RuntimeException("StartApplication Class has find.");
            }
        }
    }

    /**
     * 把 D:\work\code\20170401\search-class\target\classes\com\baibin\search\a\A.class 这样的绝对路径转换为全类名com.baibin.search.a.A
     */
    public String convertClassPath(String classPath,String allClassPath){
        return classPath.replace(allClassPath.replace("/","\\").replaceFirst("\\\\",""),"").replace("\\",".").replace(".class","");
    }
}
