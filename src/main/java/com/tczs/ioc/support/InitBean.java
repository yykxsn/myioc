package com.tczs.ioc.support;

import com.tczs.ioc.annotation.Autowired;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;

/**
 * 查询已经存放的bean，这个bean上是否有@Autowired依赖注解，
 * 如果有，为这个变量重新赋值。
 * @author TaoerLi
 *
 */
public class InitBean {

    public void initAll() throws IllegalAccessException, ClassNotFoundException {
        BeanScanPackage sp = BeanScanPackage.getBeanScanPackage();
        Iterator<Map.Entry<Class<?>, Object>> iterator = LoadBean.beanMap.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Class<?>, Object> entry = iterator.next();
            init(entry.getValue());
        }
    }

    public void init(Object obj) throws IllegalAccessException {
        Class clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for(Field field : fields){
            if(!field.isAccessible()){
                field.setAccessible(true);
            }
            if(field.isAnnotationPresent(Autowired.class)){
                if(field.get(obj) != null){
                    throw new RuntimeException("can not have init value!");
                }
                field.set(obj,LoadBean.getBean(field.getType()));
            }
        }
    }

}
