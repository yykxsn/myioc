package com.tczs.ioc.support;

import com.tczs.ioc.annotation.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 查询所有扫描到的类，有@Component注解的创建bean并且存储起来
 * @author TaoerLi
 *
 */
public  class LoadBean {

    public static Map<Class<?>,Object> beanMap = new HashMap<>();

    public static <T> T getBean(Class<T> clazz){
        Object bean = beanMap.get(clazz);
        if(bean == null){
            return null;
        }
        return (T) bean;
    }

    public void loadBean(List<Class> classList) throws IllegalAccessException, InstantiationException {

        for(Class clazz : classList){
            if(clazz.isAnnotationPresent(Component.class)){
                Object obj = clazz.newInstance();
                LoadBean.beanMap.put(clazz,obj);
            }
        }

    }

}
