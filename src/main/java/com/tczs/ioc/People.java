package com.tczs.ioc;

import com.tczs.ioc.annotation.Autowired;
import com.tczs.ioc.annotation.Component;

@Component
public class People {

    @Autowired
    private Cat cat;

    @Autowired
    private Dog dog;

    private String name;

    private int age;

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void listenCatSay(){
        System.out.println("my name is "+name+", age is "+age);
        System.out.println("i want to listen cat say:");
        cat.say();
        dog.say();
    }
}
